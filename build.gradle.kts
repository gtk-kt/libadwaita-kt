plugins {
	kotlin("multiplatform") version "1.6.0"
	`maven-publish`
}

group = "org.gnome.libadwaita"
version = "1.0.0.alpha"

repositories {
	mavenCentral()
}

kotlin {
	val hostOs = System.getProperty("os.name")
	val isMingwX64 = hostOs.startsWith("Windows")
	@Suppress("UNUSED_VARIABLE")
	val nativeTarget = when {
		hostOs == "Mac OS X" -> macosX64("native")
		hostOs == "Linux" -> linuxX64("native")
		isMingwX64 -> mingwX64("native")
		else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
	}

	linuxX64("native") {
		val main by compilations.getting
		@Suppress("UNUSED_VARIABLE")
		val adwaita by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		@Suppress("UNUSED_VARIABLE")
		val nativeMain by getting {
			dependencies {
				implementation("org.gtk-kt:gtk:+")
			}
		}
		@Suppress("UNUSED_VARIABLE")
		val nativeTest by getting
	}
}
