# Libadwaita Kt

Building blocks for modern GNOME applications in Kotlin

## Community
[Matrix](https://matrix.to/#/#gtk-kt:matrix.org)

## License
libadwaita-kt is AGPLv3

## Project status
This project is a proof of concept that such can be created. After gtk-kt stabalizes I can make this a bigger reality, especially since libadwaita is still in its alpha and is ever changing
