package org.gnome.libadwaita.adwaita

import adwaita.AdwSqueezerTransitionType
import adwaita.AdwSqueezerTransitionType.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.SqueezerTransitionType.html">
 *     AdwSqueezerTransitionType</a>
 */
enum class SqueezerTransitionType(
	val adw: AdwSqueezerTransitionType
) {
	NONE(ADW_SQUEEZER_TRANSITION_TYPE_NONE),
	CROSSFADE(ADW_SQUEEZER_TRANSITION_TYPE_CROSSFADE);

	companion object {

		fun valueOf(adw: AdwSqueezerTransitionType): SqueezerTransitionType =
			when (adw) {
				ADW_SQUEEZER_TRANSITION_TYPE_NONE -> NONE
				ADW_SQUEEZER_TRANSITION_TYPE_CROSSFADE -> CROSSFADE
			}
	}
}