package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.Display
import org.gtk.gdk.Display.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.KGObject

/**
 * libadwaita-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1.0.0.alpha.4/class.StyleManager.html">
 *     AdwStyleManager</a>
 */
class StyleManager(val styleManagerPointer: AdwStyleManager_autoptr) : KGObject(styleManagerPointer.reinterpret()) {

	var colorScheme: ColorScheme
		get() = ColorScheme.valueOf(adw_style_manager_get_color_scheme(styleManagerPointer))
		set(value) = adw_style_manager_set_color_scheme(styleManagerPointer, value.adw)

	val isDark: Boolean
		get() = adw_style_manager_get_dark(styleManagerPointer).bool

	val display: Display?
		get() = adw_style_manager_get_display(styleManagerPointer).wrap()

	val isHighContrast: Boolean
		get() = adw_style_manager_get_high_contrast(styleManagerPointer).bool

	val systemSupportsColorsSchemes: Boolean
		get() = adw_style_manager_get_system_supports_color_schemes(styleManagerPointer).bool


	companion object {

		fun getDefault(): StyleManager =
			adw_style_manager_get_default()!!.wrap()

		fun getForDisplay(display: Display): StyleManager =
			adw_style_manager_get_for_display(display.displayPointer)!!.wrap()

		inline fun AdwStyleManager_autoptr?.wrap() = this?.wrap()
		inline fun AdwStyleManager_autoptr.wrap() = StyleManager(this)

	}
}