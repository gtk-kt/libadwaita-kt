package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.widgets.Widget

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.PreferencesPage.html">
 *     AdwPreferencesPage</a>
 */
class PreferencesPage(
	val pagePointer: AdwPreferencesPage_autoptr
) : Widget(pagePointer.reinterpret()) {

	constructor() : this(adw_preferences_page_new()!!.reinterpret())

	fun add(group: PreferencesGroup) {
		adw_preferences_page_add(pagePointer, group.groupPointer)
	}

	var iconName: String?
		get() = adw_preferences_page_get_icon_name(pagePointer)?.toKString()
		set(value) = adw_preferences_page_set_icon_name(pagePointer, value)

	var pageName: String?
		get() = adw_preferences_page_get_name(pagePointer)?.toKString()
		set(value) = adw_preferences_page_set_name(pagePointer, value)

	var title: String?
		get() = adw_preferences_page_get_title(pagePointer)?.toKString()
		set(value) = adw_preferences_page_set_title(pagePointer, value)

	var useUnderline: Boolean
		get() = adw_preferences_page_get_use_underline(pagePointer).bool
		set(value) = adw_preferences_page_set_use_underline(
			pagePointer,
			value.gtk
		)

	fun remove(group: PreferencesGroup) {
		adw_preferences_page_remove(pagePointer, group.groupPointer)
	}

	companion object{

		inline fun AdwPreferencesPage_autoptr?.wrap() =
			this?.wrap()

		inline fun AdwPreferencesPage_autoptr.wrap() =
			PreferencesPage(this)
	}
}