package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ExpanderRow.html">
 *     AdwExpanderRow</a>
 */
class ExpanderRow(
	val expanderRowPointer: AdwExpanderRow_autoptr
) : PreferencesRow(expanderRowPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, ADW_TYPE_EXPANDER_ROW))

	constructor() : this(adw_expander_row_new()!!.reinterpret())

	fun addAction(widget: Widget) {
		adw_expander_row_add_action(
			expanderRowPointer,
			widget.widgetPointer
		)
	}

	fun addPrefix(widget: Widget) {
		adw_expander_row_add_prefix(
			expanderRowPointer,
			widget.widgetPointer
		)
	}

	fun addRow(child: Widget) {
		adw_expander_row_add_row(
			expanderRowPointer,
			child.widgetPointer
		)
	}

	var enableExpansion: Boolean
		get() = adw_expander_row_get_enable_expansion(
			expanderRowPointer
		).bool
		set(value) = adw_expander_row_set_enable_expansion(
			expanderRowPointer,
			value.gtk
		)

	var expanded: Boolean
		get() = adw_expander_row_get_expanded(
			expanderRowPointer
		).bool
		set(value) = adw_expander_row_set_expanded(
			expanderRowPointer,
			value.gtk
		)

	var iconName: String?
		get() = adw_expander_row_get_icon_name(
			expanderRowPointer
		)?.toKString()
		set(value) = adw_expander_row_set_icon_name(
			expanderRowPointer,
			value
		)

	var enableSwitch: Boolean
		get() = adw_expander_row_get_show_enable_switch(
			expanderRowPointer
		).bool
		set(value) = adw_expander_row_set_show_enable_switch(
			expanderRowPointer,
			value.gtk
		)

	var subtitle: String?
		get() = adw_expander_row_get_subtitle(
			expanderRowPointer
		)?.toKString()
		set(value) = adw_expander_row_set_subtitle(
			expanderRowPointer,
			value
		)
}