package org.gnome.libadwaita.adwaita

import adwaita.*
import gtk.GdkRectangle
import kotlinx.cinterop.*
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.gtk

/**
 * 21 / 11 / 2021
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/iface.Swipeable.html">
 *     AdwSwipeable</a>
 */
interface Swipeable {
	val swipeablePointer: AdwSwipeable_autoptr

	val cancelProgress: Double
		get() = adw_swipeable_get_cancel_progress(swipeablePointer)

	val distance: Double
		get() = adw_swipeable_get_distance(swipeablePointer)

	val progress: Double
		get() = adw_swipeable_get_progress(swipeablePointer)

	fun getSnapPotions(): Array<Double> =
		memScoped {
			val nSnapPoints = cValue<IntVar>()
			val cArray = adw_swipeable_get_snap_points(swipeablePointer, nSnapPoints)
			Array(nSnapPoints.ptr.pointed.value) {
				cArray?.get(it) ?: 0.0
			}
		}

	fun getSwipeArea(navigationDirection: NavigationDirection, isDrag: Boolean): Rectangle =
		memScoped {
			val rect = alloc<GdkRectangle>()
			adw_swipeable_get_swipe_area(
				swipeablePointer,
				navigationDirection.adw,
				isDrag.gtk,
				rect.ptr
			)
			rect.ptr.wrap()
		}
}