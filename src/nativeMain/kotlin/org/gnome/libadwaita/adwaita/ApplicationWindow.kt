package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Application
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.ApplicationWindow

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ApplicationWindow.html">
 *     AdwApplicationWindow</a>
 */
class ApplicationWindow(
	val applicationWindowPointer: AdwApplicationWindow_autoptr
) : ApplicationWindow(applicationWindowPointer.reinterpret()) {

	constructor(instance: TypeInstance) :
			this(
				typeCheckInstanceCastOrThrow(
					instance,
					ADW_TYPE_APPLICATION_WINDOW
				)
			)

	constructor(application: Application) :
			this(
				adw_application_window_new(
					application.applicationPointer
				)!!.reinterpret()
			)

	var content: Widget?
		get() = adw_application_window_get_content(
			applicationWindowPointer
		).wrap()
		set(value) = adw_application_window_set_content(
			applicationWindowPointer,
			value?.widgetPointer
		)
}