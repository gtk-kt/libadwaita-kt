package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.Texture
import org.gtk.gdk.Texture.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Avatar.html">
 *     AdwAvatar</a>
 */
class Avatar(
	val avatarPointer: AdwAvatar_autoptr
) : Widget(avatarPointer.reinterpret()) {

	constructor(
		size: Int,
		text: String?,
		showInitials: Boolean
	) : this(
		adw_avatar_new(
			size,
			text,
			showInitials.gtk
		)!!.reinterpret()
	)

	constructor(instance: TypeInstance) :
			this(
				typeCheckInstanceCastOrThrow(
					instance,
					ADW_TYPE_AVATAR
				)
			)

	fun drawToTexture(scaleFactor: Int): Texture =
		adw_avatar_draw_to_texture(avatarPointer, scaleFactor)!!.wrap()

	var customImage: Paintable?
		get() = adw_avatar_get_custom_image(avatarPointer).wrap()
		set(value) = adw_avatar_set_custom_image(
			avatarPointer,
			value?.paintablePointer
		)

	var iconName: String?
		get() = adw_avatar_get_icon_name(avatarPointer)?.toKString()
		set(value) = adw_avatar_set_icon_name(avatarPointer, value)

	var showInitials: Boolean
		get() = adw_avatar_get_show_initials(avatarPointer).bool
		set(value) = adw_avatar_set_show_initials(avatarPointer, value.gtk)

	var size: Int
		get() = adw_avatar_get_size(avatarPointer)
		set(value) = adw_avatar_set_size(avatarPointer, value)

	var text: String?
		get() = adw_avatar_get_text(avatarPointer)?.toKString()
		set(value) = adw_avatar_set_text(avatarPointer, value)
}