package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwCenteringPolicy.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.CenteringPolicy.html">
 *     AdwCenteringPolicy</a>
 */
enum class CenteringPolicy(
	val adw: AdwCenteringPolicy
) {
	LOOSE(ADW_CENTERING_POLICY_LOOSE),
	STRICT(ADW_CENTERING_POLICY_STRICT)
	;

	companion object {

		fun valueOf(adw: AdwCenteringPolicy): CenteringPolicy =
			when (adw) {
				ADW_CENTERING_POLICY_LOOSE -> LOOSE
				ADW_CENTERING_POLICY_STRICT -> STRICT
			}
	}
}