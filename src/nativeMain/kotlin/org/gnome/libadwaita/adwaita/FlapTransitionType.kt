package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwFlapTransitionType
import adwaita.AdwFlapTransitionType.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.FlapTransitionType.html">
 *     AdwFlapTransitionType</a>
 */
enum class FlapTransitionType(
	val adw: AdwFlapTransitionType
) {
	OVER(ADW_FLAP_TRANSITION_TYPE_OVER),
	UNDER(ADW_FLAP_TRANSITION_TYPE_UNDER),
	SLIDE(ADW_FLAP_TRANSITION_TYPE_SLIDE);

	companion object {

		fun valueOf(adw: AdwFlapTransitionType): FlapTransitionType =
			when (adw) {
				ADW_FLAP_TRANSITION_TYPE_OVER -> OVER
				ADW_FLAP_TRANSITION_TYPE_UNDER -> UNDER
				ADW_FLAP_TRANSITION_TYPE_SLIDE -> SLIDE
			}
	}
}