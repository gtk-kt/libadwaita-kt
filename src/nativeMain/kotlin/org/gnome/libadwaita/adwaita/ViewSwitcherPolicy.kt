package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwViewSwitcherPolicy
import adwaita.AdwViewSwitcherPolicy.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.ViewSwitcherPolicy.html">
 *     AdwViewSwitcherPolicy</a>
 */
enum class ViewSwitcherPolicy(
	val adw: AdwViewSwitcherPolicy
) {
	NARROW(ADW_VIEW_SWITCHER_POLICY_NARROW),
	WIDE(ADW_VIEW_SWITCHER_POLICY_WIDE)
	;

	companion object {

		fun valueOf(adw: AdwViewSwitcherPolicy): ViewSwitcherPolicy =
			when (adw) {
				ADW_VIEW_SWITCHER_POLICY_NARROW -> NARROW
				ADW_VIEW_SWITCHER_POLICY_WIDE -> WIDE
			}
	}
}