package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Window

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Window.html">
 *     AdwWindow</a>
 */
open class Window(
	val adwWindowPointer: AdwWindow_autoptr
) : Window(adwWindowPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, ADW_TYPE_WINDOW))

	constructor() : this(adw_window_new()!!.reinterpret())

	var content: Widget?
		get() = adw_window_get_content(
			adwWindowPointer
		).wrap()
		set(value) = adw_window_set_content(
			adwWindowPointer,
			value?.widgetPointer
		)
}