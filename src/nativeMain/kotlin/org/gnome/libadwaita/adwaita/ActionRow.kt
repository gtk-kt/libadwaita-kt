package org.gnome.libadwaita.adwaita

import adwaita.*
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.TypedNoArgFunc
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ActionRow.html">
 *     AdwActionRow</a>
 */
open class ActionRow(
	val actionRowPointer: AdwActionRow_autoptr
) : PreferencesRow(actionRowPointer.reinterpret()) {

	constructor(instance: TypeInstance) :
			this(typeCheckInstanceCastOrThrow(instance, ADW_TYPE_ACTION_ROW))

	constructor() : this(adw_action_row_new()!!.reinterpret())

	fun activateRow() {
		adw_action_row_activate(actionRowPointer)
	}

	fun addPrefix(widget: Widget) {
		adw_action_row_add_prefix(actionRowPointer, widget.widgetPointer)
	}

	fun addSuffix(widget: Widget) {
		adw_action_row_add_suffix(actionRowPointer, widget.widgetPointer)
	}

	var activatableWidget: Widget?
		get() = adw_action_row_get_activatable_widget(actionRowPointer).wrap()
		set(value) = adw_action_row_set_activatable_widget(
			actionRowPointer,
			value?.widgetPointer
		)

	var iconName: String?
		get() = adw_action_row_get_icon_name(actionRowPointer)?.toKString()
		set(value) = adw_action_row_set_icon_name(actionRowPointer, value)

	var subtitle: String?
		get() = adw_action_row_get_subtitle(actionRowPointer)?.toKString()
		set(value) = adw_action_row_set_subtitle(actionRowPointer, value)

	var subtitleLines: Int
		get() = adw_action_row_get_subtitle_lines(actionRowPointer)
		set(value) = adw_action_row_set_subtitle_lines(actionRowPointer, value)

	var titleLines: Int
		get() = adw_action_row_get_title_lines(actionRowPointer)
		set(value) = adw_action_row_set_title_lines(actionRowPointer, value)

	fun remove(widget: Widget) {
		adw_action_row_remove(actionRowPointer, widget.widgetPointer)
	}

	fun addOnActivatedCallback(action: TypedNoArgFunc<ActionRow>) =
		addSignalCallback(ACTIVATED, action, staticActivatedCallback)

	companion object {
		private const val ACTIVATED = "activated"

		inline fun AdwActionRow_autoptr?.wrap() =
			this?.wrap()

		inline fun AdwActionRow_autoptr.wrap() =
			ActionRow(this)

		private val staticActivatedCallback: GCallback =
			staticCFunction { self: AdwActionRow_autoptr,
			                  data: gpointer ->
				data.asStableRef<TypedNoArgFunc<ActionRow>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()
	}
}