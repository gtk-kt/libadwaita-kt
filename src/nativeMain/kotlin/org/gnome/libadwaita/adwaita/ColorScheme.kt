package org.gnome.libadwaita.adwaita

import adwaita.AdwColorScheme
import adwaita.AdwColorScheme.*

/**
 * libadwaita-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1.0.0.alpha.4/enum.ColorScheme.html">
 *     AdwColorScheme</a>
 */
enum class ColorScheme(val adw: AdwColorScheme) {
	DEFAULT(ADW_COLOR_SCHEME_DEFAULT),
	FORCE_LIGHT(ADW_COLOR_SCHEME_FORCE_LIGHT),
	PREFER_LIGHT(ADW_COLOR_SCHEME_PREFER_LIGHT),
	PREFER_DARK(ADW_COLOR_SCHEME_PREFER_DARK),
	FORCE_DARK(ADW_COLOR_SCHEME_FORCE_DARK);

	companion object {
		fun valueOf(adw: AdwColorScheme): ColorScheme =
			when (adw) {
				ADW_COLOR_SCHEME_DEFAULT -> DEFAULT
				ADW_COLOR_SCHEME_FORCE_LIGHT -> FORCE_LIGHT
				ADW_COLOR_SCHEME_PREFER_LIGHT -> PREFER_LIGHT
				ADW_COLOR_SCHEME_PREFER_DARK -> PREFER_DARK
				ADW_COLOR_SCHEME_FORCE_DARK -> FORCE_DARK
			}
	}
}