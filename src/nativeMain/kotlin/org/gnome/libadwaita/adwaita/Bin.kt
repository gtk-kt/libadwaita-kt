package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Bin.html">
 *     AdwBin</a>
 */
class Bin(
	val binPointer: AdwBin_autoptr
) : Widget(binPointer.reinterpret()) {

	constructor() : this(adw_bin_new()!!.reinterpret())

	constructor(instance: TypeInstance) :
			this(
				typeCheckInstanceCastOrThrow(
					instance,
					ADW_TYPE_BIN
				)
			)

	var child: Widget?
		get() = adw_bin_get_child(binPointer).wrap()
		set(value) = adw_bin_set_child(binPointer, value?.widgetPointer)
}