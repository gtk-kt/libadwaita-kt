package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwLeafletTransitionType
import adwaita.AdwLeafletTransitionType.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.LeafletTransitionType.html">
 *     AdwLeafletTransitionType</a>
 */
enum class LeafletTransitionType(
	val adw: AdwLeafletTransitionType
) {
	OVER(ADW_LEAFLET_TRANSITION_TYPE_OVER),
	UNDER(ADW_LEAFLET_TRANSITION_TYPE_UNDER),
	SLIDE(ADW_LEAFLET_TRANSITION_TYPE_SLIDE);

	companion object {

		fun valueOf(adw: AdwLeafletTransitionType): LeafletTransitionType =
			when (adw) {
				ADW_LEAFLET_TRANSITION_TYPE_OVER -> OVER
				ADW_LEAFLET_TRANSITION_TYPE_UNDER -> UNDER
				ADW_LEAFLET_TRANSITION_TYPE_SLIDE -> SLIDE
			}
	}
}