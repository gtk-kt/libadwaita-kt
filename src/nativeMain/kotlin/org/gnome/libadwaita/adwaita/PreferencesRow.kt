package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.ListBox
import org.gtk.gtk.widgets.Widget

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.PreferencesRow.html">
 *     AdwPreferencesRow</a>
 */
open class PreferencesRow(
	val preferencesRowPointer: AdwPreferencesRow_autoptr
) : ListBox.Row(preferencesRowPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, ADW_TYPE_PREFERENCES_ROW))

	constructor() : this(adw_preferences_row_new()!!.reinterpret())

	var title: String
		get() =
			adw_preferences_row_get_title(preferencesRowPointer)!!.toKString()
		set(value) = adw_preferences_row_set_title(preferencesRowPointer, value)

	var useUnderline: Boolean
		get() = adw_preferences_row_get_use_underline(
			preferencesRowPointer
		).bool
		set(value) = adw_preferences_row_set_use_underline(
			preferencesRowPointer,
			value.gtk
		)
}