package org.gnome.libadwaita.adwaita

import adwaita.AdwNavigationDirection
import adwaita.AdwNavigationDirection.*

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.NavigationDirection.html">
 *     AdwNavigationDirection</a>
 */
enum class NavigationDirection(
	val adw: AdwNavigationDirection
) {
	BACK(ADW_NAVIGATION_DIRECTION_BACK),
	FORWARD(ADW_NAVIGATION_DIRECTION_FORWARD);

	companion object {
		fun valueOf(adw: AdwNavigationDirection) =
			when (adw) {
				ADW_NAVIGATION_DIRECTION_BACK -> BACK
				ADW_NAVIGATION_DIRECTION_FORWARD -> FORWARD
			}
	}
}