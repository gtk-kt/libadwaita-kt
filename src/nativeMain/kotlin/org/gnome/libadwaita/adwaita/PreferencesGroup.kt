package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gtk.widgets.Widget

/**
 * 15 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.PreferencesGroup.html">
 *     AdwPreferencesGroup</a>
 */
class PreferencesGroup(
	val groupPointer: AdwPreferencesGroup_autoptr
) : Widget(groupPointer.reinterpret()) {

	constructor() : this(adw_preferences_group_new()!!.reinterpret())

	fun add(child: Widget) {
		adw_preferences_group_add(groupPointer, child.widgetPointer)
	}

	var description: String?
		get() = adw_preferences_group_get_description(groupPointer)?.toKString()
		set(value) = adw_preferences_group_set_description(groupPointer, value)

	var title: String?
		get() = adw_preferences_group_get_title(groupPointer)?.toKString()
		set(value) = adw_preferences_group_set_title(groupPointer, value)

	fun remove(child: Widget) {
		adw_preferences_group_remove(groupPointer, child.widgetPointer)
	}
}