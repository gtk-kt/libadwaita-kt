package org.gnome.libadwaita.adwaita

import adwaita.*
import org.gtk.glib.bool
import org.gtk.gtk.widgets.Widget



/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.get_enable_animations.html">
 *     adw_get_enable_animations</a>
 */
val Widget.enableAnimations: Boolean
	get() = adw_get_enable_animations(widgetPointer).bool

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.get_major_version.html">
 *     adw_get_major_version</a>
 */
fun getMajorVersion(): UInt = adw_get_major_version()

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.get_micro_version.html">
 *     adw_get_major_version</a>
 */
fun getMicroVersion(): UInt = adw_get_major_version()

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.get_major_version.html">
 *     adw_get_major_version</a>
 */
/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.get_major_version.html">
 *     adw_get_major_version</a>
 */
fun getMinorVersion(): UInt = adw_get_major_version()

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.init.html">
 *     adw_init</a>
 */
fun init() {
	ADW_MAJOR_VERSION
	adw_init()
}

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.is_initialized.html">
 *     adw_is_initialized</a>
 */
val isInitialized: Boolean
	get() = adw_is_initialized().bool