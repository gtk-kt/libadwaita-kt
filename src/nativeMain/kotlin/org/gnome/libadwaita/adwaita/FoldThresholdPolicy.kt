package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwFoldThresholdPolicy
import adwaita.AdwFoldThresholdPolicy.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.FoldThresholdPolicy.html">
 *     AdwFoldThresholdPolicy</a>
 */
enum class FoldThresholdPolicy(
	val adw: AdwFoldThresholdPolicy
) {
	MINIMUM(ADW_FOLD_THRESHOLD_POLICY_MINIMUM),
	NATURAL(ADW_FOLD_THRESHOLD_POLICY_NATURAL);

	companion object {

		fun valueOf(adw: AdwFoldThresholdPolicy): FoldThresholdPolicy =
			when (adw) {
				ADW_FOLD_THRESHOLD_POLICY_MINIMUM -> MINIMUM
				ADW_FOLD_THRESHOLD_POLICY_NATURAL -> NATURAL
			}
	}
}