package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gnome.libadwaita.adwaita.PreferencesPage.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.widgets.Widget

/**
 * 15 / 01 / 2022
 *
 * @see <a href="file:///usr/share/doc/libadwaita-1/class.PreferencesWindow.html">
 *     AdwPreferencesWindow</a>
 */
class PreferencesWindow(
	val preferencePointer: AdwPreferencesWindow_autoptr
) : Window(preferencePointer.reinterpret()) {

	constructor() : this(adw_preferences_window_new()!!.reinterpret())

	fun add(page: PreferencesPage) {
		adw_preferences_window_add(
			preferencePointer,
			page.pagePointer
		)
	}

	fun closeSubpage() {
		adw_preferences_window_close_subpage(preferencePointer)
	}

	var canSwipeBack: Boolean
		get() = adw_preferences_window_get_can_swipe_back(preferencePointer).bool
		set(value) = adw_preferences_window_set_can_swipe_back(
			preferencePointer,
			value.gtk
		)

	var searchEnabled: Boolean
		get() = adw_preferences_window_get_search_enabled(preferencePointer).bool
		set(value) = adw_preferences_window_set_search_enabled(
			preferencePointer,
			value.gtk
		)

	val visiblePage: PreferencesPage?
		get() = adw_preferences_window_get_visible_page(preferencePointer).wrap()

	val visiblePageName: String?
		get() = adw_preferences_window_get_visible_page_name(preferencePointer)?.toKString()

	fun presentSubpage(subpage: Widget) {
		adw_preferences_window_present_subpage(
			preferencePointer,
			subpage.widgetPointer
		)
	}

	fun remove(page: PreferencesPage) {
		adw_preferences_window_remove(
			preferencePointer,
			page.pagePointer
		)
	}
}