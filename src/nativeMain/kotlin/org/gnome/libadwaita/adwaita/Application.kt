package org.gnome.libadwaita.adwaita

import adwaita.*
import gio.GApplicationFlags
import kotlinx.cinterop.reinterpret
import org.gnome.libadwaita.adwaita.StyleManager.Companion.wrap
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Application

/**
 * libadwaita-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1.0.0.alpha.4/class.Application.html">
 *     AdwApplication</a>
 */
class Application(val adwApplicationPointer: AdwApplication_autoptr) :
	Application(adwApplicationPointer.reinterpret()) {

	constructor(instance: TypeInstance) :
			this(
				typeCheckInstanceCastOrThrow(
					instance,
					ADW_TYPE_APPLICATION
				)
			)

	constructor(applicationId: String, flags: GApplicationFlags) :
			this(adw_application_new(applicationId, flags)!!)

	val styleManager: StyleManager
		get() = adw_application_get_style_manager(adwApplicationPointer)!!.wrap()
}