package org.gnome.libadwaita.adwaita

import adwaita.*
import gobject.GObject
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap
import org.gtk.gtk.itemfactory.ListItemFactory
import org.gtk.gtk.itemfactory.ListItemFactory.Companion.wrap
import org.gtk.gtk.widgets.Widget

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ComboRow.html">
 *     AdwComboRox</a>
 */
class ComboRow(
	val comboBoxPointer: AdwComboRow_autoptr
) : ActionRow(comboBoxPointer.reinterpret()) {
	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, ADW_TYPE_COMBO_ROW))

	constructor() : this(adw_combo_row_new()!!.reinterpret())

	var expression: Expression?
		get() = adw_combo_row_get_expression(comboBoxPointer).wrap()
		set(value) = adw_combo_row_set_expression(
			comboBoxPointer,
			value?.expressionPointer
		)

	var factory: ListItemFactory?
		get() = adw_combo_row_get_factory(comboBoxPointer).wrap()
		set(value) = adw_combo_row_set_factory(
			comboBoxPointer,
			value?.listItemFactoryPointer
		)

	var listFactory: ListItemFactory?
		get() = adw_combo_row_get_list_factory(comboBoxPointer).wrap()
		set(value) = adw_combo_row_set_list_factory(
			comboBoxPointer,
			value?.listItemFactoryPointer
		)

	var model: ListModel?
		get() = adw_combo_row_get_model(comboBoxPointer).wrap()
		set(value) = adw_combo_row_set_model(
			comboBoxPointer,
			value?.listModelPointer
		)

	var selected: UInt
		get() = adw_combo_row_get_selected(comboBoxPointer)
		set(value) = adw_combo_row_set_selected(comboBoxPointer, value)

	val selectedItem: KGObject?
		get() = adw_combo_row_get_selected_item(comboBoxPointer)
			?.reinterpret<GObject>().wrap()

	var useSubtitle: Boolean
		get() = adw_combo_row_get_use_subtitle(comboBoxPointer).bool
		set(value) = adw_combo_row_set_use_subtitle(comboBoxPointer, value.gtk)
}