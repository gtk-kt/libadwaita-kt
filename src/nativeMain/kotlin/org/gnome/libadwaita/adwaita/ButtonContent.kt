package org.gnome.libadwaita.adwaita

import adwaita.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.TypeInstance
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ButtonContent.html">
 *     AdwButtonContent</a>
 */
class ButtonContent(
	val buttonContentPointer: AdwButtonContent_autoptr
) : Widget(buttonContentPointer.reinterpret()) {

	constructor() : this(adw_button_content_new()!!.reinterpret())

	constructor(instance: TypeInstance) :
			this(
				typeCheckInstanceCastOrThrow(
					instance,
					ADW_TYPE_BUTTON_CONTENT
				)
			)

	var iconName: String
		get() = adw_button_content_get_icon_name(
			buttonContentPointer
		)!!.toKString()
		set(value) = adw_button_content_set_icon_name(
			buttonContentPointer,
			value
		)

	var label: String
		get() = adw_button_content_get_label(
			buttonContentPointer
		)!!.toKString()
		set(value) = adw_button_content_set_label(
			buttonContentPointer,
			value
		)

	var useUnderline: Boolean
		get() = adw_button_content_get_use_underline(buttonContentPointer).bool
		set(value) = adw_button_content_set_use_underline(
			buttonContentPointer,
			value.gtk
		)

}