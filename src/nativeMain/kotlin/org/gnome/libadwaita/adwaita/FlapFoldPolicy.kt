package org.gnome.libadwaita.adwaita

import adwaita.AdwCenteringPolicy
import adwaita.AdwFlapFoldPolicy
import adwaita.AdwFlapFoldPolicy.*

/**
 * 22 / 01 / 2022
 *
 * @see <a href="https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/enum.FlapFoldPolicy.html"></a>
 */
enum class FlapFoldPolicy(
	val adw: AdwFlapFoldPolicy
) {
	NEVER(ADW_FLAP_FOLD_POLICY_NEVER),
	ALWAYS(ADW_FLAP_FOLD_POLICY_ALWAYS),
	AUTO(ADW_FLAP_FOLD_POLICY_AUTO);

	companion object {

		fun valueOf(adw: AdwFlapFoldPolicy): FlapFoldPolicy =
			when (adw) {
				ADW_FLAP_FOLD_POLICY_NEVER -> NEVER
				ADW_FLAP_FOLD_POLICY_ALWAYS -> ALWAYS
				ADW_FLAP_FOLD_POLICY_AUTO ->AUTO
			}
	}
}